import setuptools

setuptools.setup(
    name='anjoz_browser',
    packages=setuptools.find_packages(),
    description='Browser configurations and BasePage objects for use in Anjoz projects',
    version='0.1',
    author='Breno @Anjoz',
    author_email='brenoaps435@gmail.com',
    url="https://gitlab.com/BrenoAlberto/anjoz_browser",
)
