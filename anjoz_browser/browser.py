from selenium import webdriver

import anjoz_browser.utils as utils

import os
import stat

def init_chrome_browser():
    pathBrowser = utils.get_path_from_root_dir("bin/chromedriver.exe")
    # st = os.stat(pathBrowser)
    # os.chmod(pathBrowser, st.st_mode | stat.S_IEXEC)
    chrome_options = webdriver.ChromeOptions()
    # binaryPathBrowser = utils.get_path_from_root_dir("bin/chrome-linux/chrome")
    # st = os.stat(binaryPathBrowser)
    # os.chmod(binaryPathBrowser, st.st_mode | stat.S_IEXEC)
    # chrome_options.binary_location = binaryPathBrowser
    # chrome_options.add_argument("--headless")
    chrome_options.add_argument("--disable-extensions")
    chrome_options.add_argument("--disable-infobars")
    chrome_options.add_argument("--disable-plugins-discovery")
    #chrome_options.add_argument("--start-maximized")
    #chrome_options.add_argument("--disable-gpu")
    chrome_options.add_argument("--disable-dev-shm-usage")
    chrome_options.add_argument("--no-sandbox")
    chrome_options.add_experimental_option("excludeSwitches", ["enable-automation"])
    chrome_options.add_experimental_option("useAutomationExtension", False)
    chrome_options.add_argument(
        "--user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) "
        "Chrome/73.0.3683.86 Safari/537.36 "
    )
    chrome_options.add_argument("--log-level=3")
    chrome_options.add_experimental_option("excludeSwitches", ["enable-logging"])
    chrome_browser = webdriver.Chrome(
        executable_path=pathBrowser,
        chrome_options=chrome_options,
    )
    chrome_browser.delete_all_cookies()

    return chrome_browser


def quit_browser(browser):
    browser.quit()
